#!/usr/bin/env python

"""
C2OPENMP parser
"""

import re
import sys
import getopt
import os

def read_file(input_file):
    try:
        ifile = open(input_file,'r')
    except IOError:
        print('\nI/O error in c2openmp.py. Verify your input/output files.\n')
        exit()
    return ifile.readlines()

def usage():
    print('\nUsage: -i --input=  --> input_file')
    print('         -p --preprocessed= --> preprocessed file')
    print('         -o --output= --> output file')
    exit()

def opt_reader():
    verbose = False
    try:
        options, remainder = getopt.getopt(sys.argv[1:],
                                           'i:o:p:', ['input=', 'output=', 'preprocessed='])
    except getopt.GetoptError as err:
        print( str(err))
        usage()

    if(options == []):
        usage()

    o_file = i_file = p_file = ''

    for opt,arg in options:
        if opt in ('-o', '--output'):
            o_file = arg
            continue
        if opt in ('-i', '--input'):
            i_file = arg
            continue
        if opt in ('-p', '--preprocessed'):
            p_file = arg
            continue

    opt = {'preprocessed' : p_file,
           'input'   : i_file,
           'output'  : o_file}

    return opt

def main():

    options     = opt_reader()
    input_file  = options['input']
    preprocessed_file  = options['preprocessed']
    output_file = options['output']

    #handle the preprocssed file
    lines = read_file(preprocessed_file)
    USER_DEFINED  = False
    INTERNAL  = False
    internals = []
    for k,line in enumerate(lines):
        if not INTERNAL:
            if USER_DEFINED:
                search = re.search("\s*//<\\\\USER_DEFINED>",line)
                if search:
                    USER_DEFINED = False
                    continue
            search = re.search("\s*//<USER_DEFINED>",line)
            if search:
                USER_DEFINED = True
                continue

        #We now store the internal variables....
        search = re.search("\s*//<INTERNAL>",line)
        if search:
            INTERNAL = True

        if INTERNAL:
            search = re.search("^\s*[//]",line)
            if not search:
                search = re.search("__attribute__",line)
                if search:
                    search = re.search("__attribute__\(\(unused\)\)(\w+)",line)
                    if search:
                         internals.append(search.group(1))
                    else:
                         print("Bad internals line found by c2omp.py: ",line)
                         exit()
                else:
                    search = re.search("\s*[^//](\w+)\s+(\w+)",line)
                    if search:
                         internals.append(search.group(2))

        search = re.search("\s*//<\\\\INTERNAL>",line)
        if search:
            INTERNAL = False

    #We get the lines from the input file
    lines = read_file(input_file)
    #We open the output file
    ofile = open(output_file,"w")

    LOOP      = False
    skip      = False
    MAIN_LOOP = False

    output_lines = []

    for k,line in enumerate(lines):

        search = re.search("\s*//<\\\\INCLUDES>",line)
        if search:
            skip = True

        #We get the function name.
        #Warning... not comments with the same structure allowed...
        search = re.search("\s*(\w+)\s+(\w+)_cpu\s*\(",line)
        if search:
            skip = False
            output_lines.append(line.replace("cpu","omp"))
            continue

        if skip:
            continue

        #000000000000000000000000000000000000000000000000000000000
#
#        #We now store the internal variables....
#        search = re.search("\s*//<INTERNAL>",line)
#        if search:
#            INTERNAL = True
#
#        if INTERNAL:
#            #We search type and name of internal variables
#            #We first check for ifdefs....
#            if re.search("\s*#if.*",line):
#                output_lines.append(line)
#                continue
#
#            search = re.search("^\s*[//]",line)
#            if not search:
#                search = re.search("\s*[^//](\w+)\s+(\w+)",line)
#                if search:
#                    internals.append(search.group(2))
#
#        search = re.search("\s*//<\\\\INTERNAL>",line)
#        if search:
#            INTERNAL = False

        #000000000000000000000000000000000000000000000000000000000
        search = re.search("\s*//<MAIN_LOOP>",line)
        if search:
            MAIN_LOOP = True

        if MAIN_LOOP:
            #We now add the internals to the prama and the omp for
            #decorator on top of the first loop
            #search = re.search("\s*for\s+.*{",line) #CM this regexp requires a space after the for
            search = re.search("\s*for\s*.*{",line)
            if search and not LOOP:
                for kk in range(1,len(lines)):
                    search = re.search(";",output_lines[-kk])
                    if search:
                        temp_line = "#pragma omp parallel firstprivate("
                        for _internal in internals[:-1]:
                            temp_line += _internal+","
                        temp_line += internals[-1]+") \n{\n"
                        output_lines.insert(-kk+1,temp_line+"#pragma omp for\n")
                        LOOP = True
                        break
        output_lines.append(line)

        search = re.search("\s*//<\\\\MAIN_LOOP>",line)
        if search:
            output_lines.append("\n}\n") #We close the final statement....
            continue


    for line in output_lines:
        ofile.write(line)
    ofile.close()

main()
