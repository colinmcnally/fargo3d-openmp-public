# FARGO3D #

#### A versatile MULTIFLUID HD/MHD code that runs on clusters of CPUs or GPUs, with special emphasis on protoplanetary disks. 


![example](https://bytebucket.org/fargo3d/public/raw/81b497fb327e916d1c2ad650fe1177b1bbbcc1de/utils/images/fargo3d.jpg)
------------------------

##### Website: [fargo.in2p3.fr](http://fargo.in2p3.fr)

##### [Documentation](https://fargo3d.bitbucket.io/)

------------------------

##### Clone

HTTPS: ```git clone https://bitbucket.org/fargo3d/public.git```

SSH (bitbucket user required): ```git clone git@bitbucket.org:fargo3d/public.git```

##### Fork & Sync:

Follow the bitbucket documentation [here](https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html)

##### Working with different versions

Version 2.0: Switch to the commit tagged as ``2.0`` to work with version 2.0.

```
git checkout -b branch2.0 2.0
```

Version 1.3: Switch to the commit tagged as ``1.3`` to work with version 1.3.

```
git checkout -b branch1.3 1.3
```

##### Contributing to the code

[Pull requests](https://www.atlassian.com/git/tutorials/making-a-pull-request) are available to the branch ``release/public``. 

Bugs can be reported to the [issues section](https://bitbucket.org/fargo3d/public/issues) of the repository or to the [Google group](https://groups.google.com/forum/#!forum/fargo3d).

### First run

#### Sequential CPU

``` 
make SETUP=fargo PARALLEL=0 GPU=0
./fargo3d in/fargo.par
```

#### Parallel CPU

```
make SETUP=fargo PARALLEL=1 GPU=0
mpirun -np 4 ./fargo3d in/fargo.par
```

#### Sequential GPU

```
make SETUP=fargo PARALLEL=0 GPU=1
./fargo3d in/fargo.par
```

#### Parallel GPU

```
make SETUP=fargo PARALLEL=1 GPU=1
mpirun -np 2 ./fargo3d in/fargo.par
```

------------------------

### Description of subdirectories:

* planets: library of planetary systems.

* scripts: python scripts needed to build the code.

* setups: this is where all the custom setup definition are stored. The name of the setups correspond to the names of the directories found in setups/

* src: this is where all the source files of the code are found. Some of them may be redefined in the setups/ subdirectory (the makefile uses the VPATH variable, which behaves much like the PATH variable of the shell, as it allows to decide in which order a given source file is sought within different directories).

* std: this is where all the files that contain some standard definitions (everything that is not   a source file, not a script, and that the user is not supposed to modify). This includes, for   instance, the definition of the some standard boundary conditions, the units (scaling rules) of   the code parameters, etc.

* test_suite: contains python scripts that are used to test various features of the code. They are invoked as follows. We take the example of the permut.py script (which tests that the output of the Orszag-Tang vortex is independent of the choice of coordinates, XY, YZ or XZ). In the main directory (parent directory of test_suite/), simply issue: make testpermut The rule is therefore simply to issue: make test[name of python script without extension] for any script found in this subdirectory. All these scripts should use the 'test' python module found in scripts/

* utils: contains some utilities to post-process the data.

--------------------------
### OpenMP support:

This is the result of work by Colin McNally and Pablo Benitez-Llambay.

The concept is to abuse the change_arch system in FARGO3D to switch to OpenMP version of what would otherwise be GPU kernels. The addition of OpenMP pragmas is done for most of the code in an automatic way, by tansforming the source through scripts/c2omp.py. Reductions have manually added OpenMPi pragmas. c2omp.py exploits the markup for c2cuda to produce a list of firstprivate variables for a OpenMP for region around the main loops of a kernel.

The main use of OpenMP/MPI hybrid parallelization with FARGO3D is to get scaling of large problems on CPU clusters with thousands of cores and fat multi-core nodes. You will need to experiment to find the right MPI rank / OpenMP thread ratio. You will also need to make sure that each OpenMP thread is on a separate core, and that things are organized the best way by NUMA region on each cluster you run on. Performance can vary by more than a factor of ten on a given machine/problem depending of configuration.

Testing: This version produces bit-identical output for the fargo and otvortex tests with gcc at least. To check for yourself with your configuration/problem, run with CPU and OMP selected in std/func_arch.cfg and check the md5 sums for pairs of output files.

Sample benchmarking results:

#### Small problems, varying the MPI/OpenMP mix

Machine:

Infiniband EDR (2:1 blocking), per node 2x18 core Intel SkyLake CPUs – Xeon 6140 2.3Ghz

Intel Compilers, flags -xCORE-AVX2 -O3

Intel MPI 

Setup p3diso with grid 5000x64x64, ~66^3 per core, 2 nodes

Gas update speeds (cell updates per second)

| Configuration                 | Speed  |
|-------------------------------|--------|
| 72 processes,  1 thread each  | 1.42e7 |
| 36 processes,  2 threads each | 1.75e7 |
| 18 processes,  4 threads each | 1.94e7 |
|  8 processes,  9 threads each | 2.20e7 |
|  4 processes, 18 threads each | 2.07e7 |
|  4 processes, 17 threads each | 1.94e7 |

Setup p3diso with grid 10000x256x128, ~166^3 per core, 2 nodes

Gas update speeds (cell updates per second)

| Configuration                 | Speed  |
|-------------------------------|--------|
| 72 processes,  1 thread each  | 2.66e7 |
| 36 processes,  2 threads each | 3.01e7 |
| 18 processes,  4 threads each | 3.09e7 |
|  8 processes,  9 threads each | 3.05e7 |
|  4 processes, 18 threads each | 2.77e7 |
|  4 processes, 17 threads each | 2.82e7 |

#### Weak parallel scaling

Scaling varies based on the mix for MPI ranks and OpenMP threads. 
The plot below shows weak scaling (constant work per core) normalized to the speed for pure MPI on one node. 
At the highest core counts, leaving a core unoccupied per socket (17 threads per rank) begins to win out. 
Results will vary based on CPU, interconnect, MPI library, problem and grid size (at least).

![Weak scaling](https://bitbucket.org/colinmcnally/fargo3d-openmp-public/raw/1a2eee30f74a065d80f9617a01755b3ca508fab4/openmp_weak_scaling.png)
