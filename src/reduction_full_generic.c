real name_full_reduction (macro) (Field *F, int ymin, int ymax, int zmin, int zmax) {
  int j,k;
  real *reduc2d;
  real result;
  real local_result;
  reduc2d = Reduction2D->field_cpu;

  name_reduction(macro) (F, ymin, ymax, zmin, zmax);

  INPUT2D (Reduction2D);
  
  result = INIT_REDUCTION(macro);
#pragma omp parallel private(j,k,local_result)
  {
    local_result = INIT_REDUCTION(macro);
#pragma omp for
    for (k = zmin; k < zmax; k++) {
      for (j = ymin; j < ymax; j++) {
	local_result = macro(reduc2d[l2D], local_result);
      }
    }
#pragma omp critical
    result = macro(local_result, result);
  }
  return result;
}
